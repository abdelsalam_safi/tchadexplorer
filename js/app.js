L.Icon.Default.imagePath = "css/leaflet/images"

$(document).on('pageinit', '#index', function () {

    /* ************** Map init ************** */
    var map = L.map('map', {zoomControl: false});

    /*var tilelayer = L.tileLayer('http://localhost:20008/tile/hdm/{z}/{x}/{y}.png', { */
    var tilelayer = L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
        maxZoom: 20,
        attribution: "OpenStreetMap Contributors"
    }).addTo(map);

    map.setView([12.1177, 15.0674], 13);

    $(document).on('pageshow', '#index', function () {
        $('#map')[0].style.height = window.innerHeight + "px";
        map.invalidateSize();
        $('#map')[0].style.height = "100%";
        /* Locate on page show */
         map.locate({
            maxZoom: 18,
            setView: true,
            timeout: 3000
        });
    });

    /* ************** Map listeners ************** */
    function onLocationFound(e) {
        var radius = e.accuracy / 2;
        var smallCircle = L.circle(e.latlng, 2).addTo(map);
        var bigCircle = L.circle(e.latlng, radius).addTo(map);
        window.setTimeout(function () {
            smallCircle.remove();
            bigCircle.remove();
        }, 5000);
    }
    map.on('locationfound', onLocationFound);



    /* ************ Icon Managment ************* */
    function pointToLayer(feature, latlng){
        var path = "images/default.png";
        if(feature.properties.tourism ==="hotel"){
            path = "images/hotel.png";
        }
        else if(feature.properties.amenity ==="hopital"){
                path = "images/hotel.png";
        }
        else if(feature.properties.amenity ==="school"){
            path = "images/school.png";
        }
        else if(feature.properties.amenity ==="museum"){
                path = "images/museum.png";
               }
        else if(feature.properties.amenity ==="embassy"){
                path = "images/embassy.png";
               }
        else if(feature.properties.amenity ==="place_of_worship"){
            if(feature.properties.religion === "christian"){
                path = "images/christian.png";
            }
            else if(feature.properties.religion === "muslim"){
                path = "images/muslim.png";
            }
        }
        var icon = L.icon({
            iconUrl : path,
            iconSize: [32, 37], //Pour centrer le popup
            popupanchor: [0, -16]
        });
        return L.marker(latlng, {icon: icon});
    }

    //Recuperer Les POIs d'OpenStreetMap et les afficher sur la carte
    function onEachFeature(feature, marker){
        var content = "", value;
        if(feature.properties.name){
            content= "<h3>" + feature.properties.name + "</h3>";
        }
        for(var key in feature.properties){
            if(key === "@id"){
                continue;
            }
            content += "<p><strong>" + key + ": </strong>" + feature.properties[key] + "</p>";
        }
        marker.bindPopup(content);
    }

    /* ************** Buttons listeners ************** */
    /*
        $('#locationButton').on('click', function () {
            map.locate({
                maxZoom: 18,
                setView: true,
                timeout: 3000
            });
        });
    */

    var options = {
        onEachFeature : onEachFeature,
        pointToLayer : pointToLayer
    };

    var worshipGroup = L.geoJson(null, options);
    var hotelGroup = L.geoJson(null, options);

    $('#worshipButton').on('click', function(){
        toggleLayer(worshipGroup, 'data/place_of_worship.geojson', '#worshipButton');
    });

    $('#hotelButton').on('click', function(){
        toggleLayer(hotelGroup, 'data/hotel.geojson', '#hotelButton');
    });

    function toggleLayer(group, path, buttonId){
        if(map.hasLayer(group)){
            map.removeLayer(group);
            $(buttonId).removeClass("on");
        }
        else{
            if(!group.getLayers().length){
                $.getJSON(path, function(data){
                    group.addData(data);
                });
            }
            group.addTo(map);
            $(buttonId).addClass("on");
        }
    }

});